**Бот** - это специализированный тип пользователя. Под ним нельзя залогиниться, а управлять может создатель через InfoBot-а. Бот подчиняется общим правам системы, т.е. должен быть добавлен в чат в который должен писать/читать. Боту передается текст сообщений обращенных к нему, если задан обратный url. Читать все сообщения чата бот не может.


# Создание и управление ботом

 Cоздавать и менять настройки бота можно с помощью бота InfoBot в своем Info Chat-е.

Для создания бота используется команда `@infobot new_bot helpbot ChatHelpBot`, где:

- первый аргумент, уникальное имя, которое должно оканчиваться на `bot`, 
- второй аргумент, читаемое имя. 

В ответ придет сообщение с ключом АПИ, где

- `URL_KEY`, первые 8 символов используются при создании url бота 

- `API_KEY`, отбросим первые 20 символом и получим ключ АПИ, для отправки постов и доступа к другим АПИ (через заголовок `X-APIToken`).

### ***Команда боту***

```javascript
@infobot new_bot uniquenamebot VisibleNameBot
```
### ***Ответ бота***

```javascript
New bot @uniquenamebot created
Secret key: as3afgmkkmmepfbosv6o6hpph8s3fj2b9kpd0jutpts3i9ct67u
To start using bot you need setup callback url with command set_bot uniquenamebot url http://example.com/as3afgmk
Url must contain as3afgmk
```
API - `as3afgmkkmmepfbosv6o6hpph8s3fj2b9kpd0jutpts3i9ct67u`, где

- `URL_KEY = as3afgmk`

- `API_KEY = 6hpph8s3fj2b9kpd0jutpts3i9ct67u`

```
 as3afgmk kmmepfbosv6o 6hpph8s3fj2b9kpd0jutpts3i9ct67u
|--------|            |                               |
URL_KEY(8)            |                               |
|---------(20)--------|------------API_KEY------------|
```

```javascript
{
    `X-APIToken`: '6hpph8s3fj2b9kpd0jutpts3i9ct67u'
}
```


## Установка url, на который будут приходить обращения к боту

Команда `@infobot set_bot helpbot url http://example.com/as3afgmk`, где

- `helpbot` - уникальное имя бота, 
- `url` - редактируемая опция, 
- `http://example.com/as3afgmk` - значение опции, адрес, куда будут отправляться сообщения.


## Получение детальной информация по боту

Команда `bot_info <unique_name>` позволит узнать подробную инормацию о боте , где 

- `<unique_name>` - уникальное имя созданного бота

### ***Команда боту***

```javascript
bot_info uniquenamebot
```

### ***Ответ бота***

```javascript
@uniquenamebot
---
NAME: VisibleNameBot
RECEIVER URL:
    http://example.com/as3afgmk
    on this url you will receive messages from users
SENDING URL:
    POST https://api.pararam.io/bot/message
    BODY {"key": "6hpph8s3fj2b9kpd0jutpts3i9ct67u", "text": "example text", "chat_id": <chat_id:int>}
    with this url you will be able to send messages to chats where your bot has access
KEY: as3afgmkkmmepfbosv6o6hpph8s3fj2b9kpd0jutpts3i9ct67u
```

## Просмотр доступных команд

Команды можно посмотреть, отправив `@infobot help`

### ***Команда боту***

```javascript
@infobot help
```
### ***Ответ бота***

```javascript
bots - list your bots
new_bot <unique_name> <friendly name> - create new bot
set_bot <unique_name> <option> <value> - set option to bot
bot_new_key <unique_name> - generate new key
bot_info <unique_name> - bot details
```

# Отправка сообщений


## **Отправка сообщений в неприватный чат**

`POST /bot/message`

### ***Parameters***

No parameters

### ***Request body***
```javascript
{
  "key": "6hpph8s3fj2b9kpd0jutpts3i9ct67u"
  "chat_id": 37,  // int
  "reply_no": null,  // int or null
  "text": "@all test" // text, 10k symbols limit
}
```
### ***Responses***

200 OK

```javascript
{
  "post_no": 123
}
```

### ***Пример запроса***

```python      
response = httpx.post("https://api.pararam.io/bot/message", json={
  "key": "6hpph8s3fj2b9kpd0jutpts3i9ct67u",
  "text": "example text",
  "chat_id": 37,
    # "reply_no": 8 #ответ на сообщение
  }
)
```

## **Отправка сообщений в приватный чат**

## По user_id

`POST /msg/post/private`

### ***Parameters***

No parameters

### ***Request body***
```javascript
{
  "text": "some text",
  "user_id": 12
} 
```
### ***Responses***

200 OK

```javascript
{
  "chat_id": 23,
  "post_no": 123
}
```
### ***Пример запроса***

```python      
headers = {
  "X-APIToken": "6hpph8s3fj2b9kpd0jutpts3i9ct67u"
}       

response = httpx.post("https://api.pararam.io/msg/post/private", headers = headers, json={
  "text": "example text",
  "user_id": 12
    # "reply_no": 8 #ответ на сообщение
  }
)
```

## По email

`POST /msg/post/private`

### ***Parameters***

No parameters

### ***Request body***
```javascript
{
  "text": "some text",
  "user_email": "user@example.com"
}
```
### ***Responses***

200 OK

```javascript
{
  "chat_id": 23,
  "post_no": 123
}
```

### ***Пример запроса***

```python
headers = {
  "X-APIToken": "6hpph8s3fj2b9kpd0jutpts3i9ct67u"
}       

response = httpx.post("https://api.pararam.io/msg/post/private", headers = headers, json={
  "key": "6hpph8s3fj2b9kpd0jutpts3i9ct67u",
  "text": "example text",
  "user_email": "user@example.com"
    # "reply_no": 8 #ответ на сообщение
  }
)
```

## По уникальному имени

`POST /msg/post/private`

### ***Parameters***

No parameters

### ***Request body***
```javascript
{
  "text": "hello world!",
  "user_unique_name": "unique_name"
}
```
### ***Responses***

200 OK

```javascript
{
  "chat_id": 23,
  "post_no": 123
}
```

### ***Пример запроса***

```python
headers = {
  "X-APIToken": "6hpph8s3fj2b9kpd0jutpts3i9ct67u"
}       

response = httpx.post("https://api.pararam.io/msg/post/private", headers = headers, json={
  "key": "6hpph8s3fj2b9kpd0jutpts3i9ct67u",
  "text": "example text",
  "user_unique_name": "unique_name"
    # "reply_no": 8 #ответ на сообщение
  }
)
```

## **Отправка файлов в чат**

Бот может отправляеть файлы в чат составным запросом с помощью  **multipart/form-data**, использующийся для отправки **HTML-форм**

`POST https://file.pararam.io/new/upload`

### ***Parameters***

No parameters

### ***Request multipart/form-data***

```
chat_id = 37 &
size = {file_size}

data = {file_data}
```
### ***Responses***

200 OK

```javascript
{
  "guid": "2031c0eb0ff045c69dbbd9f9624f307b"
}
```
### ***Пример запроса***

```python
headers = {"X-APIToken": "6hpph8s3fj2b9kpd0jutpts3i9ct67u"}
payload = {"chat_id": 70737, "size": len((open("example.txt", "rb")).read())}
data = {"data": open("example.txt", "rb")}

response = httpx.post("https://file.pararam.io/new/upload", headers=headers, data=payload, files=data)
```

### ***Пример запроса curl***

```curl 
  -H "X-APIToken: ..." 
  -X POST 
  -F "size=`wc -c < ./ansible.log`" 
  -F "chat_id=37" 
  -F "data=@./ansible.log" 
https://file.pararam.io/new/upload;
```

## **Обработка ошибок**

***Ошибки доступа***

```javascript
{
  "code": "access_deny"
  "message": "...some text..."
}
```

***Ошибки валидации***

Можно определить по флагу `"tamtam_response_api": true`

```javascript
{
  "tamtam_response_api": true,
  "codes": {"<field_name>|non_field": "str_code"},
  "field_errors": {"<field_name>|non_field": "str_message"}
  "extra": {}, // optioanl
  "error": "str_message"
}
```


# Получение сообщений

На адрес установленный командой  `@infobot set_bot helpbot url http://example.com/as3afgmk`

 POST-ом отправляется сообщение в формате json

```javascript
{
  // кто обратился к боту
  "user_id": 12,  // int
  "user_unique_name": "real_user",  // str or null, unique_name по user_id
  // где было обращение (пост, чат, команда)
  "post_no": 11,  // int
  "chat_id": 21,  // int
  "organization_id": null,  // null or int
  // что было написано боту
  "text": "some text",  // str
  "text_parsed": [{"type": "text", "value": "some text"}],  // list[dict], распознанные блоки форматирования текста
  // если сообщение содержит ответ на другой пост, тут будет номер и текст этого поста
  "reply_no": 7,  // optional, int
  "reply_text": "some text",  // optional, str
  // если сообщение содержит файл, тут будет guid и имя файла
  "file_guid": "5d9d966e0dcc4cb28a86acd79323cf2b",  // optional, str (hex)
  "file_name": "example.txt",  // optional, str
  // список прикрепленных файлов (файлы будут загружены до отправки поста, куда они прикрепленны, но при обработке следует учитывать, что файл с указанным guid может отсутствовать или быть недоступен)
  "attachments": null,   // optional, list[str] - список guid файлов
}
```

и заголовок `X-Signature` - подпись пакета, ее можно проверить так:

```python
BKEY = to_bytes(int("<key>", 32))

def _make_signature(body: str):
    return str(base64.b64encode(
        hmac.new(BKEY, bytes(body, "utf8"), digestmod=hashlib.sha256).digest()
    ), "utf8")

if request.headers["x-signature"] != _make_signature(await request.text()):
    print("Check failed")
```


# Команды бота (bot actions)

В случаях когда бот имеет небольшое число команд или требуется выбор из нескольких вариантов - ввод команд в диалоговом режиме может быть слишком сложным и неочевидным для пользователя. Бот, помимо входящих сообщений может обрабатывать команды (actions), которые бот сам формирует динамически в своих сообщениях в виде ссылок специального формата (валидных url):

```bash
bot://{action}?title={title}[&{key}={value}]
```

+ action: <str: `a-z_`> - имя команды
+ title: <url valid str>  - текст, отображаемый пользователю в сообщении
+ key, value: <url valid str> - любые пользовательские параметры 
+ общая длинна строки не более 1024 символов, строки следует прогнать через url-quoting


В результате пользователь увидит ссылку (позже будут и другие варианты отрисовки), нажав на которую боту будет направлен запрос с подписанными данными:

```bash
POST https://{bot-server-url}/act
```
```javascript
{
  "user_id": <int>,  // пользователь, который нажал на action-ссылку
  "chat_id": <int>,  // чат в котором вызвана команда
  "post_no": <int>,  // пост в котором ботом была отправлена команда
  "organization_id": <int> | null  // команда в которой находиться чат
  "action": <str>  // имя команды
  "params": {<str>: <str>}  // все параметры после ?  
}
```

Следует учитывать:
- action-ссылки принимаются только от ботов (обычный пользователь не может их формировать)
- нажатие на action-ссылку не сопровождается постом или каким-либо другим образом для всех участников чата
- action-ссылки можно нажимать много раз, в групповых чатах их может прожать любой участник
- `title` и `ui` зарезервированные служебные параметры


Пример:

Текст сообщения от бота

```
Выберите доску для размещения карточек по-умолчанию:
bot://set_default_board?title=Team%20board&board_id=54623
bot://set_default_board?title=My%20board&board_id=324512
bot://set_default_board?title=Main%20board&board_id=83924
```

Запрос на сервер бота при нажатии на ссылку `My board` пользователем (user#132)

```POST https://example.com/as3afgmk/act```
```javascript
{
  "user_id": 132,
  "chat_id": 11768,
  "post_no": 45876,
  "organization_id": null,
  "action": "set_default_board",
  "params": {
    "title": "My%20board",
    "board_id": "324512"
  }
}
```


# Основные АПИ

Пробная [OpenApi3](https://tamtamfiles-public.s3-eu-west-1.amazonaws.com/openapi/v2/openapi.yaml) спецификация (могут быть отступления в ответах, ValidationError мало описан). Можно открыть в https://editor.swagger.io/ и использовать в качестве документации.

Ниже представленны некоторые из API


# Пользователи

## **Получение информации о пользователе по id**

Зная id пользователя можно запросить общую информацию о нем

`GET /core/user?ids=11,12,15`.

### ***Parameters***

ids - id пользователей

### ***Request body***

No body

### ***Responses***

200 OK

```javascript
{
  "users": [
    {
      "id": 0,
      "name": "string",
      "unique_name": "string",
      "info": "string",
      "info_parsed": [
        {}
      ],
      "deleted": true,
      "active": true,
      "time_updated": "string",
      "time_created": "string",
      "is_bot": true,
      "organizations": [
        0
      ]
    }
  ]
}
```

### ***Пример запроса***


```python
ids = ","join([11, 12, 15])

url = "https://api.pararam.io/core/user?ids={ids}"
        
headers = {
  "X-APIToken": "6hpph8s3fj2b9kpd0jutpts3i9ct67u",
}  
        
response = httpx.get(url, headers = headers)
```

## **Получение информации о пользователе по unique name**

Зная unique name пользователя можно запросить общую информацию о нем

`GET /core/user?unames=user0,user1,user2`.

### ***Parameters***

unique name - уникальное имя пользователей

### ***Request body***

No body

### ***Responses***

200 OK

```javascript
{
  "users": [
    {
      "id": 0,
      "name": "string",
      "unique_name": "string",
      "info": "string",
      "info_parsed": [
        {}
      ],
      "deleted": true,
      "active": true,
      "time_updated": "string",
      "time_created": "string",
      "is_bot": true,
      "organizations": [
        0
      ]
    }
  ]
}
```

### ***Пример запроса***


```python
unames = ","join(['user0','user1','user2'])
'
url = "https://api.pararam.io/core/user?unames={unames}"
        
headers = {
  "X-APIToken": "6hpph8s3fj2b9kpd0jutpts3i9ct67u",
}  
        
response = httpx.get(url, headers = headers)
```

# Чаты

Получить данные всех доступных чатов можно в 2 приема

## **Получение id чатов (независимо от статуса видимости)**

`GET /core/chat/sync`

### ***Parameters***

No parameters

### ***Request body***

No body

### ***Responses***

200 OK

```javascript
{
  "chats": [
    0
  ]
}
```

### ***Пример запроса***

```python

url = "https://api.pararam.io/core/chat/sync"
        
headers = {
  "X-APIToken": "6hpph8s3fj2b9kpd0jutpts3i9ct67u"
}  
        
response = httpx.get(url, headers = headers)
```

## **Получение данных чатов по id**

`GET /core/chat?ids=11,12,15`.

### ***Parameters***

ids - id чатов

### ***Request body***

No body

### ***Responses***

200 OK

```javascript
{
  "chats": [
    {
      "id": 0,
      "title": "string",
      "description": "string",
      "organization_id": 0,
      "posts_live_time": 0,
      "two_step_required": true,
      "history_mode": "string",
      "org_visible": true,
      "allow_api": true,
      "read_only": true,
      "posts_count": 0,
      "pm": true,
      "e2e": true,
      "time_created": "string",
      "time_updated": "string",
      "time_edited": "string",
      "author_id": 0,
      "tnew": true,
      "adm_flag": true,
      "custom_title": "string",
      "is_favorite": true,
      "inviter_id": 0,
      "tshow": true,
      "user_time_edited": "string",
      "history_start": 0,
      "pinned": [
        0
      ],
      "member_ids": [
        0
      ],
      "admin_ids": [
        0
      ],
      "group_ids": [
        0
      ],
      "guests": [
        0
      ],
      "thread_users": [
        0
      ],
      "thread_admins": [
        0
      ],
      "thread_groups": [
        0
      ],
      "last_msg": "string",
      "last_read_post_no": 0,
      "last_msg_author_id": 0,
      "last_msg_author": "string",
      "last_msg_bot_name": "string",
      "last_msg_text": "string"
    }
  ]
}

```

### ***Пример запроса***

```python
ids = ","join([11, 12, 15])

url = "https://api.pararam.io/core/chat?ids={ids}"
        
headers = {
  "X-APIToken": "6hpph8s3fj2b9kpd0jutpts3i9ct67u"
}  
        
response = httpx.get(url, headers = headers)
```

Бот может создавать/редактировать/удалять чаты в соответствии со своими правами.
Редактирование состава участников чата ботам не доступно.

Для работы с приватными чатами, есть отдельное АПИ `POST /core/chat/pm/{user_id}`, которое возвращает id приватного чата, существующего или вновь созданного.

Бот может входить/выходить в/из чатов, скрывать ненужные, добавлять/убирать из избранных, форкать.



# Команды

Получить данные всех доступных команд, можно в 2 приема 

## **Получение id команд**

`GET /core/org/sync` 

### ***Parameters***

No parameters

### ***Request body***

No body

### ***Responses***

200 OK

```javascript
{
  "ids": [
    0
  ]
}
```

### ***Пример запроса***

```python
url = "https://api.pararam.io/core/org/sync"
        
headers = {
  "X-APIToken": "6hpph8s3fj2b9kpd0jutpts3i9ct67u"
}  
        
response = httpx.get(url, headers = headers)
```

## **Получение данных команд по id**

`GET /core/org?ids=11,12,15`.

### ***Parameters***

ids - id команд

### ***Request body***

No body

### ***Responses***

200 OK

```javascript
{
  "orgs": [
    {
      "id": 0,
      "slug": "string",
      "title": "string",
      "description": "string",
      "description_parsed": [
        {}
      ],
      "email_domain": "string",
      "time_created": "string",
      "time_updated": "string",
      "two_step_required": true,
      "default_chat_id": 0,
      "is_member": true,
      "is_admin": true,
      "state": "string",
      "inviter_id": 0,
      "guests": [
        0
      ],
      "users": [
        0
      ],
      "admins": [
        0
      ],
      "groups": [
        0
      ]
    }
  ]
}
```

### ***Пример запроса***

```python
ids = ","join([11, 12, 15])

url = "https://api.pararam.io/core/org?ids={ids}"
        
headers = {
  "X-APIToken": "6hpph8s3fj2b9kpd0jutpts3i9ct67u"
}  
        
response = httpx.get(url, headers = headers)
```


Так же бот может создавать/редактировать/удалять команды в соответствии со своими правами.
Входить по приглашению и покидать команды. 

Редактирование состава участников чата ботам не доступно.


