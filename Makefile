SHELL := /bin/bash


all: clean build

clean:

build:
	npm install

test:
	node tests/spellcheck/spellCheck.js

check_conditional_fail:
	$(if $(FAIL_TESTS), \
		$(error "Build and test stages failed due to \
			 params, set by handler daemon. \
			 Please, check your MR for trello \
			 link in description"))

test_unattended: check_conditional_fail test

UNIQ_SORT = sort -u $(1) -o $(1)

dict:
	$(call UNIQ_SORT, tests/spellcheck/dict.en.txt)
	$(call UNIQ_SORT, tests/spellcheck/dict.ru.txt)

