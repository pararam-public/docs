# Как войти в [Pararam](https://app.pararam.io/login#login-buttons) через [Google](https://google.ru) аккаунт?
*  На главной странице [Pararam](https://app.pararam.io/login#login-buttons) нажать кнопку [Sign in with Google](https://https://accounts.google.com/signin/oauth/identifier?client_id=765450686918-sls0q62p3sj6o3nu54tjt9br6577v499.apps.googleusercontent.com&as=wAXvB2Hom53ZoQOBzC65lQ&destination=https%3A%2F%2Fapi.pararam.io&approval_state=!ChRyak9qYXhNbTRWNEFlUDdtbllJRhIfY3h1d0tYcVdKdkFYOERFdWhZOThQYy1WNG5jeXJ4WQ%E2%88%99AJDr988AAAAAXOuZDKyG2z7DZNzI3Z8IUeHuCn7_xfsF&oauthgdpr=1&xsrfsig=ChkAeAh8T1bCHI6rxacJaY9MqIhE7exdwpSEEg5hcHByb3ZhbF9zdGF0ZRILZGVzdGluYXRpb24SBXNvYWN1Eg9vYXV0aHJpc2t5c2NvcGU&flowName=GeneralOAuthFlow).

![Главный экран](../img/main_screen.png)


* В случае уже зарегистрированного ранее аккаунта "Google"

*  На открывшемся экране заполнить поле "**Телефон или адрес эл.почты**" и нажать "**Далее**"

![Гугл вход](../img/google_in.png)

*  Если аккаунта нет, нажать на кнопку [Создать аккаунт](https://accounts.google.com/signup/v2/webcreateaccount?continue=https%3A%2F%2Faccounts.google.com%2Fsignin%2Foauth%2Fconsent%3Fauthuser%3Dunknown%26part%3DAJi8hAPTwJSuvT3Nms--VHm01FS9EHOTkep_C3bNaFLwRdStphgjYbP3FBmUePgJAHivP3p1TH4Wp_Vr7hVwGxxS7RER7QI27WuiGFj7e36p7z6BwGCaCnxmm76u-hkApMMmXo89qAPO-PBJ1SdTeBZmchTSRkJMkHjv7UGm3brLgM8s-Y_e4L7LJuqVVqTIKfes2DcobiMErkomkGcs5H28N4DsLromDdkpPB6FGbmw6Qdg8jZr7w-_CNCCiNP-AYMKFu-Aoi99bzGDGCuGt31eKHNxKHHEPXkVhhFIorLxdebcm10Uzpqeld7ng8ySPsnvgZfYVZ-YN6PbDMmyE_9ijtvka8vU7l1COz09huLCMpeAEQNod7uss5dV4Qu3gKvX1AeBiuNPkbhhsJ6254eXwpVPdXDv8kp4gHVQoaeTKiabF_4RX2o%26as%3DwAXvB2Hom53ZoQOBzC65lQ%23&signInUrl=https%3A%2F%2Faccounts.google.com%2Fsignin%2Foauth%3Fclient_id%3D765450686918-sls0q62p3sj6o3nu54tjt9br6577v499.apps.googleusercontent.com%26as%3DwAXvB2Hom53ZoQOBzC65lQ%26destination%3Dhttps%253A%252F%252Fapi.pararam.io%26approval_state%3D%2521ChRyak9qYXhNbTRWNEFlUDdtbllJRhIfY3h1d0tYcVdKdkFYOERFdWhZOThQYy1WNG5jeXJ4WQ%25E2%2588%2599AJDr988AAAAAXOuZDKyG2z7DZNzI3Z8IUeHuCn7_xfsF%26oauthgdpr%3D1%26xsrfsig%3DChkAeAh8T1bCHI6rxacJaY9MqIhE7exdwpSEEg5hcHByb3ZhbF9zdGF0ZRILZGVzdGluYXRpb24SBXNvYWN1Eg9vYXV0aHJpc2t5c2NvcGU&flowName=GlifWebSignIn&flowEntry=SignUp).

* На открывшемся экране ввести пароль от Google аккаунта и нажать "**Вход**".

* Если у аккаунта "**Pararam**" есть 2х аутентификация, открывается экран ввода е2е пароля, который надо ввести чтоб выполнить вход.

![e2e](../img/e2e.png)


* Если у аккаунта нет 2х аутентификации, по нажатию "**Далее**" пользователь попадает на станицу со списком чатов в  "**Pararam**". 